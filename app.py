from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from floorPlan import FloorPlanVisualization

from flask_cors import CORS, cross_origin

app = Flask(__name__)
CORS(app, support_credentials=True)


api = Api(app)

@app.route('/', methods=['GET'])
def home():
    return '''<h1>PwC's Floor Plan</h1>'''


api.add_resource(FloorPlanVisualization, "/floorplanvisualization")


if __name__ == '__main__':
    
    # important to mention debug=True
    print("*************************** PwC Floor Plan Server Live ***************************")
    # app.run(host='0.0.0.0', port=8888)
    app.run(debug=True)
