from flask import Flask, request,make_response,send_file
from flask_restful import Resource
import os
import fnmatch
import random
from PIL import Image
from io import BytesIO

class FloorPlanVisualization(Resource):


    def post(self):
        print("Start generating floor plan for user...")
        Plotarea = request.form["plotarea"]
        bedrooms = request.form["bedrooms"]
        bhk_info = str(bedrooms) + 'bhk'
        bhk_info1 = str(bhk_info)+"*"
        dict1={'1bhk': [],'2bhk':[],"3bhk":[],"4bhk":[],"5bhk":[]}
        #imgdir = f'C:/Users/amishra318/Downloads/floor_planner/master_image_data'
        imgdir = os.path.join(os.getcwd(),'master_image_data')

        for name in list(os.scandir(imgdir)):
             #print(fnmatch.fnmatch(str(name)[11:],bhk_info1))
            if  fnmatch.fnmatch(str(name)[11:],bhk_info1):
                if bhk_info == '1bhk':
                    # image_list_1bhk.append(str(name)[11:].replace(">","").replace("'",""))
                    dict1[bhk_info].append(str(name)[11:].replace(">","").replace("'",""))
                elif bhk_info == '2bhk':
                    # image_list_2bhk.append(str(name)[11:].replace(">","").replace("'",""))
                    dict1[bhk_info].append(str(name)[11:].replace(">","").replace("'",""))
                elif bhk_info == '3bhk':
                    # image_list_3bhk.append(str(name)[11:].replace(">","").replace("'",""))
                    dict1[bhk_info].append(str(name)[11:].replace(">","").replace("'",""))
                elif bhk_info == '4bhk':
                    # image_list_4bhk.append(str(name)[11:].replace(">","").replace("'",""))
                    dict1[bhk_info].append(str(name)[11:].replace(">","").replace("'",""))
                else:
                    # image_list_5bhk.append(str(name)[11:].replace(">","").replace("'",""))
                    dict1[bhk_info].append(str(name)[11:].replace(">","").replace("'",""))


        if bhk_info in dict1.keys():
            #print(dict1[bhk_info])
            img_file = random.choice(dict1[bhk_info])
            image_path = imgdir +'/' +img_file
            print(image_path)
            im = Image.open(image_path)
            # image = im.tobytes()
            img_io = BytesIO()
            im.save(img_io, 'JPEG', quality=100)
            img_io.seek(0)
            #print(image)
            # im.show()
            # 



        response = make_response(send_file(img_io, download_name="floorplan.jpg", mimetype='image/jpeg', as_attachment=True))
        return response


